# Greenbox Bot

Dies ist der Source Code des Greenbox Bots, so wie wir ihn auf dem Discord “Greenbox Server” verwenden.

Der Greenbox Bot ist relativ einfach geschrieben und soll möglichst leichtgewichtig ausfallen. Der volle Code ist in einer einzigen Datei zu finden, welche alle Funktionen beinhaltet.

Der Bot übernimmt bei uns die Funktionsweise des “Laberfeuers”, beinhaltet zahlreiche Commands und weitere Funktionen, organisiert die Musikbots, Timeouts von Nutzern und hilft beim Verifizierungsprozess.

Dieser Bot wurde angepasst auf unseren Server geschrieben und unterstützt keine einfache Nutzung auf anderen Servern. Hierfür müsste der Programmcode gezielt angepasst werden.



## Anleitung

### Vorraussetzungen

- Python 3.4.2+
- python3-pip
- Requirements installiert

### Setup

```bash
python3 -m pip install -U -r requirements.txt
python3 setup.py
```

Info: Dem `setup.py` Skript muss ein Discord Bot Token von der Seite <https://discordapp.com/developers/applications/> übergeben werden.

### Ausführen

```bash
python3 run.py
```



## Lizenz

AGPLv3+. Bitte helfe anderen, wie dir geholfen wurde, und teile deine Codeänderungen mit der Welt!



Viel Spaß!